{ config, lib, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  imports = [
    ./machine/z13.nix
  ];

  home = {
    username      = "hans";
    homeDirectory = "/home/hans";
    stateVersion  = "23.05";
    sessionVariables = {
      EDITOR   = "vim";
      VISUAL   = "vim";
      BROWSER  = "firefox";
      TERMINAL = "alacritty";
    };
    packages = with pkgs; [
      exa lf glow bat starship zoxide tealdeer ripgrep jq tree fd procs mdcat 
      unzip zip cron tmux killall fzf btop xclip xsel xsv 
      lua brave kitty wezterm alacritty meld imagemagick

      # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })
    ];

    # Home Manager is pretty good at managing dotfiles. The primary way to manage
    # plain files is through 'home.file'.
    file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;
    };
  };

  # Enable simple programs
  programs = {
    exa.enable = true;
    zoxide.enable = true;
    starship.enable = true;
    fzf.enable = true;
    bat.enable = true;
  };

  programs.zsh = {
    enable = true;
    enableSyntaxHighlighting = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
    shellAliases = {
      cl = "clear";
      pg = "ps auxf | grep";
      em = "emacsclient -c -a emacs";
      ls = "exa -al --icons --group-directories-first";
      lt = "tree -ar";
      se = "vim ~/.config/home-manager/home.nix";
      sw = "home-manager switch";
    };
    initExtra = ''
      eval "$(zoxide init zsh)"
      eval "$(starship init zsh)"
    '';
  };

  programs.neovim = {
    enable = true;
    vimAlias = true;
    viAlias = true;
    extraConfig = lib.fileContents ../nvim/init.vim;
    plugins = with pkgs.vimPlugins; [
      #nvim-tree-lua
      vim-commentary
      vim-surround
      vim-nix
      fzf-vim
      rainbow
      gruvbox-community
      editorconfig-vim
    ];
  };


  programs.vim = {
    enable = true;
    extraConfig = ''
      noswapfile
      set nowrap
      set splitright splitbelow
      set nobackup
      set hidden
      colorscheme  onedark
    '';
    settings = {
      relativenumber = true;
      number = true;
      tabstop = 2;
    };
    plugins = with pkgs.vimPlugins; [
      vim-commentary
      vim-surround
      vim-nix
      fzf-vim
      rainbow
      gruvbox-community
      onedark-vim
    ];
  };

  programs.alacritty = {
    enable = true;
    settings = {
      windows.opacity = 0.85;
      live_config_reload = true;
      font = {
        #normal.familiy = "jetbrains-mono";
        size = 9;
      };
    };
  };

  programs.kitty = {
    enable = true;
    # https://github.com/kovidgoyal/kitty-themes/blob/master/themes.json
    font = {
      name = "Monaco";
      size = 14;
    };
    keybindings = {
      "kitty_mod+e" = "kitten hints"; # https://sw.kovidgoyal.net/kitty/kittens/hints/
    };
    settings = {
      # https://github.com/kovidgoyal/kitty/issues/371#issuecomment-1095268494
      # mouse_map = "left click ungrabbed no-op";
      # Ctrl+Shift+click to open URL.
      confirm_os_window_close = "0";
      # https://github.com/kovidgoyal/kitty/issues/847
      macos_option_as_alt = "yes";
    };
  };
 
  #github.com/jeaye/nix-files/blob/master/workstation/user/jeaye/data/dotfiles/tmux.conf
  programs.tmux = {
    enable = true;
    clock24 = true;
    shortcut = "a";
    baseIndex = 1;
    escapeTime = 0;

    plugins = with pkgs; [
      tmuxPlugins.better-mouse-mode
    ];

    extraConfig = ''
      unbind-key C-b
      set-option -g prefix C-a
      bind-key C-a

      set -g default-terminal "xterm-256color"
      set-option -g mouse on
      bind | split-window -h -c "#{pane_current_path}"
      bind - split-window -v -c "#{pane_current_path}"
      bind c new-window -c "#{pame_current_path}"

      bind h select-pane -L
      bind j select-pane -D
      bind k select-pane -U
      bind l select-pane -R

      bind-key J resize-pane -D 5
      bind-key K resize-pane -U 5
      bind-key H resize-pane -L 5
      bind-key L resize-pane -R 5

      set-window-option -g mode-keys vi
      bind P paste-buffer
    '';
  };


  programs.git = {
    enable = true;
    userEmail = "hans@hahoweb.nl";
    userName = "Hans Hofman";
    signing.key = "GPG-KEY-ID";
    signing.signByDefault = false;
    extraConfig = {
      init.defaultBranch = "main";
    };
    aliases = {
      a  = "add";
      co = "checkout";
      c  = "commit";
      b  = "branch";
      f  = "fetch";
      p  = "push";
    };
    ignores = [
      ".direnv"
      "flake.nix"
      "flake.lock"
      ".emvrc"
      ".data"
      "vendor"
    ];
  };

}

