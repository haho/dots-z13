{ config, pkgs, ... }:
let
  user = "hans";
in
{
  imports =
    [  
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Swap
  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  networking.hostName = "nixos";        # Define your hostname.
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "nl_NL.UTF-8";
      LC_IDENTIFICATION = "nl_NL.UTF-8";
      LC_MEASUREMENT = "nl_NL.UTF-8";
      LC_MONETARY = "nl_NL.UTF-8";
      LC_NAME = "nl_NL.UTF-8";
      LC_NUMERIC = "nl_NL.UTF-8";
      LC_PAPER = "nl_NL.UTF-8";
      LC_TELEPHONE = "nl_NL.UTF-8";
      LC_TIME = "nl_NL.UTF-8";
    };
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    xkbVariant = "";
  };

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Fonts
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    hack-font
    jetbrains-mono
    font-awesome
  ];

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Enable CUPS
  services.printing = {
    enable = true;
    drivers = with pkgs; [hplip];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${user} = {
    isNormalUser = true;
    description = "Hans Hofman";
    extraGroups = [ "networkmanager" "wheel" "lp" "scanner" "video" "audio" ];
    shell = pkgs.zsh;
  };

  programs.zsh.enable = true;

  # Omit sudo password
  security.sudo.wheelNeedsPassword = false;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # System  profile
  environment = {
    systemPackages = with pkgs; [
      vim_configurable
      wget
      exa
      tree
      git
      gnupg
      pass
      firefox
      nixos-icons
    ];
    interactiveShellInit = ''
      alias sb='sudo nixos-rebuild switch'
      alias sc='sudo vim /etc/nixos/configuration.nix'
      alias sv='sudo vim'
      alias sr='sudo reboot'
      alias ls='exa -al --icons --group-directories-first'
      alias lt='tree -ar'
    '';
  };

  # Auto Channel upgrade
  system.autoUpgrade = {
    enable = true;
    channel = "https://nixos.org/channel/nixos-unstable";
  };

  # Store maintenanc
  # manually: [sudo] nix-collect-garbage -d
  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-then 7d";
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Mount local archive
  fileSystems."/home/${user}/archive" = {
    device = "//192.168.178.70/Archive";
    fsType = "cifs";
    options = let
      automount_opts="x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    in ["${automount_opts},credentials=/etc/nixos/.smbsecrets,uid=1000,gid=1000"];
  };

  system.stateVersion = "23.05"; # Did you read the comment?

}
